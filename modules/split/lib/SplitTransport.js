import BasicTransport from "@oddlog/transport-inherit-basic";
import NestedTransport from "@oddlog/transport-inherit-nested";
import {MAPPED as LEVELS} from "@oddlog/levels";

/*===================================================== Exports  =====================================================*/

export default SplitTransport;

/*===================================================== Classes  =====================================================*/

/**
 * Creates a new SplitTransport for the given logger that forwards logs according to split levels.
 *
 * @param {Object} options The options to pass to {@link BasicTransport} and {@link NestedTransport}.
 * @param {Function} createTransport A function to create new Transport instances from transport definition objects.
 * @param {Number[]} splitLevels The levels to split at.
 *
 * @param {Boolean} [createTransports] Passed to {@link NestedTransport}.
 * @constructor
 */
function SplitTransport(options, createTransport, splitLevels, createTransports) {
  BasicTransport.call(this, options, LEVELS.silent);
  NestedTransport.call(this, options, createTransport, createTransports, true, null);
  this.splitLevels = splitLevels;
}

// extend BasicTransport
SplitTransport.prototype._neglect = BasicTransport.prototype._neglect;
SplitTransport.prototype._isShy = BasicTransport.prototype._isShy;
// extend NestedTransport
SplitTransport.prototype.attach = NestedTransport.prototype.attach;
SplitTransport.prototype.postChild = NestedTransport.prototype.postChild;
SplitTransport.prototype.forceChild = NestedTransport.prototype.forceChild;

/**
 * Creates a child SplitTransport instance.
 *
 * @param {Boolean} hasDedicatedScope Whether the childLogger has a dedicated scope.
 * @returns {SplitTransport} The child Transport.
 */
SplitTransport.prototype.child = function (hasDedicatedScope) {
  const child = new SplitTransport(this._options, this._createTransport, this.splitLevels, false);
  child.postChild(this, hasDedicatedScope);
  return child;
};

/**
 * Forwards the log to nested transports according to the defined split thresholds.
 *
 * @param {Log} log The log to process.
 */
SplitTransport.prototype.write = function (log) {
  const splitLevels = this.splitLevels;
  for (let i = 0; i < splitLevels.length; i++) {
    if (log.level < splitLevels[i]) {
      this.transports[i].write(log);
      return;
    }
  }
  this.transports[splitLevels.length].write(log);
};

/**
 * Returns the minimum level of logs to be processed by this transport.
 * For this transport type, this will return `max(this.level, transports[0].minLevel())`.
 *
 * @returns {Number} The minimum level of logs to be processed by this transport.
 */
SplitTransport.prototype.minLevel = function () {
  const t0minLevel = this.transports[0].minLevel();
  return this.level < t0minLevel ? t0minLevel : this.level;
};
