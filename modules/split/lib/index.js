import {getLevelValue} from "@oddlog/levels";

import SplitTransport from "./SplitTransport";

/*===================================================== Exports  =====================================================*/

export default SplitTransport;

export const plugin = mkPlugin();
export {create, mkPlugin};

/*==================================================== Functions  ====================================================*/

/**
 * Creates a new SplitTransport according to given options.
 *
 * @param {Object} options The options:
 * <ul>
 *   <li>`{(Number|String)[]}` split The levels to split at. The length must equal the length of `transports` minus one.
 *       Logs with a level between `thresholds[i-1]` (inclusive) and `thresholds[i]` (exclusive) will be passed to
 *       `transports[i]`; with `thresholds[-1] := 0`, `thresholds[thresholds.length] := Infinity`.</li>
 *   <li>Additional options as accepted by {@link NestedTransport}.</li>
 *   <li>Additional options as accepted by {@link BasicTransport}.</li>
 * </ul>
 * @param {Function} createTransport A function to create new Transport instances from transport definition objects.
 * @returns {SplitTransport} The created transport.
 */
function create(options, createTransport) {
  if (options.split.length !== options.transports.length - 1) {
    throw new Error("The amount of split levels must equal the amount of nested transports minus one.");
  }
  const split = options.split.map(getLvlValue);

  return new SplitTransport(options, createTransport, split, true);
}

function getLvlValue(nameOrValue) { return getLevelValue(nameOrValue, null); }

function check(options) { return options.hasOwnProperty("split"); }

function mkPlugin(options) {
  const overwrite = typeof options === "boolean" ? options : !!(options && options.overwrite);
  return (oddlog) => oddlog.defineType("split", create, check, overwrite);
}
