#!/usr/bin/env node

import os from "os";
import fs from "fs";
import {pipe, trapSignals} from "@oddlog/cli-utils";
import {getLevelValue} from "@oddlog/levels";

const streams = [];
const split = [];

/*==================================================== Execution  ====================================================*/

trapSignals();
parseArgs(process.argv.slice(2));
pipe({
  output: streams[0],
  onLog: handleLog,
  onEnd: endStreams,
});

/*==================================================== Functions  ====================================================*/

function parseArgs(argv) {
  if (argv.length === 1) {
    // Usage 1
    streams.push(process.stdout, process.stderr);
    split.push(lvlValue(2));
  } else {
    // Usage 2
    streams.push(fs.createWriteStream(argv[0]));
    for (let i = 1; i < argv.length; i += 2) {
      split.push(lvlValue(i));
      if (i + 1 < argv.length) { streams.push(fs.createWriteStream(argv[i + 1])); }
    }
  }
}

function lvlValue(i) {
  let levelValue = getLevelValue(process.argv[i], null);
  if (levelValue == null) {
    printHelp(process.stderr);
    process.exit(1);
  }
  return levelValue;
}

function handleLog(log) {
  const level = log.level;
  let i = 0;
  for (; i < split.length; i++) { if (level >= split[i]) { break; } }
  const stream = streams[i];
  if (stream) {
    stream.write(log);
    stream.write(os.EOL);
  }
}

function endStreams() { streams.forEach((stream) => stream && stream !== process.stdout && stream.close()); }

function printHelp(stream) {
  stream.write("Usage 1: oddlog-split LEVEL" + os.EOL);
  stream.write("Usage 2: oddlog-split FILE LEVEL FILE [LEVEL FILE...]" + os.EOL);
  stream.write(os.EOL);
  stream.write("Usage 1 will split by that level and output to stdout and stderr respectively." + os.EOL);
}
