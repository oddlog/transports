import fs from "fs";
import {EOL} from "os";
import StreamTransport from "@oddlog/transport-stream";

/*===================================================== Exports  =====================================================*/

export default FileTransport;

/*===================================================== Classes  =====================================================*/

/**
 * Creates a new FileTransport that writes log records into a file.
 *
 * @param {Object} options The options to pass to {@link StreamTransport}.
 * @param {String} path The file path to write to.
 * @constructor
 */
function FileTransport(options, path) {
  const stream = fs.createWriteStream(path, {flags: "a"});
  // todo remove duplication with StreamTransport.create
  const format = options.format;
  const separator = options.hasOwnProperty("separator") ? options.separator : EOL;
  const encoding = options.encoding;
  StreamTransport.call(this, options, stream, true, format || "raw", separator, encoding);
  this.stream.on("error", (err) => this.logger.emit("error", err));
}

// extend StreamTransport
FileTransport.prototype.minLevel = StreamTransport.prototype.minLevel;
FileTransport.prototype.attach = StreamTransport.prototype.attach;
FileTransport.prototype.child = StreamTransport.prototype.child;
FileTransport.prototype.write = StreamTransport.prototype.write;
FileTransport.prototype._neglect = StreamTransport.prototype._neglect;
FileTransport.prototype._isShy = StreamTransport.prototype._isShy;
FileTransport.prototype._endStream = StreamTransport.prototype._endStream;
