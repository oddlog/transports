import FileTransport from "./FileTransport";

/*===================================================== Exports  =====================================================*/

export default FileTransport;

export const plugin = mkPlugin();
export {create, mkPlugin};

/*==================================================== Functions  ====================================================*/

/**
 * Creates a new FileTransport according to given options.
 *
 * @param {Object} options The options:
 * <ul>
 *   <li>`{String} path` The file path to write to.</li>
 *   <li>Additional options as accepted by {@link StreamTransport.create}. Except for `stream`, `closeStream` and
 *       `format`.</li>
 * </ul>
 * @returns {FileTransport} The created transport.
 */
function create(options) {
  const path = options.path;

  return new FileTransport(options, path);
}

function check(options) { return options.hasOwnProperty("path"); }

function mkPlugin(options) {
  const overwrite = typeof options === "boolean" ? options : !!(options && options.overwrite);
  return (oddlog) => oddlog.defineType("file", create, check, overwrite);
}
