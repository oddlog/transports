import {getLevelValue, MAPPED as LEVELS} from "@oddlog/levels";
import {create as createCache} from "@oddlog/cache";

import TriggerTransport from "./TriggerTransport";

/*===================================================== Exports  =====================================================*/

export default TriggerTransport;

export const plugin = mkPlugin();
export {create, mkPlugin};

/*==================================================== Functions  ====================================================*/

/**
 * Creates a new TriggerTransport according to given options.
 *
 * @param {Object} options The options:
 * <ul>
 *   <li>`{Number|String} [trigger=ERROR]` The trigger level needed to initiate a cache flood.</li>
 *   <li>`{Boolean} [guard=true]` Whether to wrap the cached logs with guard logs (`--- start/end cache ---`).</li>
 *   <li>`{Boolean|Number} [replicate=false]` Whether to create dedicated caches for children. If a number is
 *       provided, children of higher nesting share the cache with their ancestors.</li>
 *   <li>Additional options as accepted by {@link createCache}.</li>
 *   <li>Additional options as accepted by {@link DeferredTransport}.</li>
 *   <li>Additional options as accepted by {@link NestedTransport}.</li>
 *   <li>Additional options as accepted by {@link BasicTransport}.</li>
 * </ul>
 * @param {Function} createTransport A function to create new Transport instances from transport definition objects.
 * @returns {TriggerTransport} The created transport.
 */
function create(options, createTransport) {
  const trigger = getLevelValue(options.trigger, LEVELS.error);
  const guard = options.guard !== false;
  const replicate = options.replicate || false;
  const cache = createCache(options);
  // create new transport
  return new TriggerTransport(options, trigger, guard, createTransport, cache, replicate, true);
}

function check(options) { return options.hasOwnProperty("trigger") || options.hasOwnProperty("replicate"); }

function mkPlugin(options) {
  const overwrite = typeof options === "boolean" ? options : !!(options && options.overwrite);
  return (oddlog) => oddlog.defineType("trigger", create, check, overwrite);
}
