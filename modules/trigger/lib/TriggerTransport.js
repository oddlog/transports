import Log from "@oddlog/record";
import DeferredTransport from "@oddlog/transport-inherit-deferred";
import NestedTransport from "@oddlog/transport-inherit-nested";
import BasicTransport from "@oddlog/transport-inherit-basic";
import {MAPPED as LEVELS} from "@oddlog/levels";

let floodingId = 0;

/*===================================================== Exports  =====================================================*/

export default TriggerTransport;

/*===================================================== Classes  =====================================================*/

/**
 * A Transport class that stores logs in a cache queue until a trigger level is satisfied. In that case, all cached logs
 * are forwarded to nested transports.
 *
 * @class
 */

/**
 * Creates a new TriggerTransport for the given logger.
 *
 * @param {Object} options The options to pass to {@link BasicTransport}, {@link DeferredTransport} and
 *        {@link NestedTransport}.
 * @param {Number} trigger The trigger level needed to initiate a cache flooding.
 * @param {Boolean} guard Whether to wrap floods with guarding logs.
 * @param {Function} createTransport A function to create new Transport instances from transport definition objects.
 * @param {RayCache|RingCache} cache The cache to use.
 * @param {Number|Boolean} [replicate] Whether to create dedicated caches for children. If a number is provided,
 *        children of higher nesting share the cache with their ancestors.
 * @param {Boolean} [createTransports] Passed to {@link NestedTransport}.
 * @constructor
 */
function TriggerTransport(options, trigger, guard, createTransport, cache, replicate, createTransports) {
  BasicTransport.call(this, options, LEVELS.silent);
  NestedTransport.call(this, options, createTransport, createTransports, false, null);
  DeferredTransport.call(this, this._options);
  this.trigger = trigger;
  this.guard = guard;
  this.forceChild = true;
  this.replicate = replicate;
  this._cache = cache;
}

// extend BasicTransport
TriggerTransport.prototype._isShy = BasicTransport.prototype._isShy;
TriggerTransport.prototype._neglect = BasicTransport.prototype._neglect;
// extend DeferredTransport
TriggerTransport.prototype.preWrite = DeferredTransport.prototype.preWrite;
// extend NestedTransport
TriggerTransport.prototype.minLevel = NestedTransport.prototype.minLevel;
TriggerTransport.prototype._write = NestedTransport.prototype.write;
TriggerTransport.prototype.attach = NestedTransport.prototype.attach;
TriggerTransport.prototype.postChild = NestedTransport.prototype.postChild;

/**
 * Creates a child TriggerTransport instance.
 *
 * @param {Boolean} hasDedicatedScope Whether the childLogger has a dedicated scope.
 * @returns {TriggerTransport} The child Transport.
 */
TriggerTransport.prototype.child = function (hasDedicatedScope) {
  if (!hasDedicatedScope && (this.replicate === false)) { return this; }
  const replicate = typeof this.replicate !== "number" ? this.replicate :
      this.replicate >= 1 ? this.replicate - 1 : false;
  const cache = replicate === false ? this._cache : this._cache.child();
  let child = new TriggerTransport(
      this._options, this.trigger, this.guard, this._createTransport, cache, replicate, false
  );
  return child.postChild(this, hasDedicatedScope);
};

/**
 * Processes a log if it is not to be ignored. If the log level is above the trigger level of the Transport, it triggers
 * a flood procedure {@link TriggerTransport#_trigger}. Otherwise, the log gets appended to the cache queue.
 *
 * @param {Log} log The log to handle.
 */
TriggerTransport.prototype.write = function (log) {
  if (this._neglect(log)) { return; }
  if (log.level < this.trigger) {
    // not triggered; add to cache
    this._cache.push(this.preWrite(log));
  } else {
    // triggered; flood cache
    this._trigger("Trigger satisfied.", log);
  }
};

/**
 * Initiates a flooding procedure. This will write all cached logs into all nested Transports. It will also create
 * trace records to surround the block of cached logs.
 *
 * @param {String} reason The reason for the flood trigger.
 * @param {Log} log The current log that triggered the flood.
 * @param {Object} [data] Additional data to pass to end guard payload.
 * @private
 */
TriggerTransport.prototype._trigger = function (reason, log, data) {
  const logger = this.logger;
  const id = floodingId++;
  const date = Date.now();
  const payload = {id, reason, flood: null, data};

  logger.emit("transport:trigger:flooding:start", payload, this);

  // write pre-flood log
  if (this.guard) {
    this._write(new Log(logger, date, LEVELS.trace, null, [true, {id}, "--- start cache ---"], false));
  }

  // flood cached logs
  payload.flood = this._cache.flood(log, this._write.bind(this));

  // create post-flood log
  if (this.guard) {
    this._write(new Log(logger, date, LEVELS.trace, null, [true, payload, "--- end cache ---"], false));
  }

  logger.emit("transport:trigger:flooding:end", payload, this);
};
