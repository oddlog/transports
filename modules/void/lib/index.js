import VoidTransport from "./VoidTransport";

let instance = null;

/*===================================================== Exports  =====================================================*/

export default VoidTransport;

export const plugin = mkPlugin();
export {create, mkPlugin};

/*==================================================== Functions  ====================================================*/

/**
 * Returns a singleton VoidTransport.
 *
 * @returns {VoidTransport} The singleton VoidTransport.
 */
function create() {
  if (instance == null) { instance = new VoidTransport(); }
  return instance;
}

function mkPlugin(options) {
  const overwrite = typeof options === "boolean" ? options : !!(options && options.overwrite);
  return (oddlog) => oddlog.defineType("void", create, overwrite);
}
