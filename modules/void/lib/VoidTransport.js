export default VoidTransport;

/*===================================================== Classes  =====================================================*/

/**
 * A Transport class that does nothing.
 *
 * @constructor
 */
function VoidTransport() {}

VoidTransport.prototype.attach = VoidTransport.prototype.write = function () {};

VoidTransport.prototype.child = function () { return this; };

VoidTransport.prototype.minLevel = function () { return Number.POSITIVE_INFINITY; };
