import BasicTransport from "@oddlog/transport-inherit-basic";
import {valueToName} from "@oddlog/levels";

import {consoleFnByLevel} from "./levels";

const __attach = BasicTransport.prototype.attach;

/*===================================================== Exports  =====================================================*/

export default ConsoleTransport;

/*===================================================== Classes  =====================================================*/

/**
 * Creates a new ConsoleTransport that writes messages to a stream.
 *
 * @param {!Object} options The options to pass to {@link BasicTransport}.
 * @constructor
 */
function ConsoleTransport(options) {
  if (!options.hasOwnProperty("shy")) { options.shy = true; }
  BasicTransport.call(this, options);
  this._loggerName = null;
}

// extend BasicTransport
ConsoleTransport.prototype.minLevel = BasicTransport.prototype.minLevel;
ConsoleTransport.prototype._neglect = BasicTransport.prototype._neglect;
ConsoleTransport.prototype._isShy = BasicTransport.prototype._isShy;

/**
 * Attaches this transport to the passed logger.
 * This means using that loggers `shy` value as fallback and its name for the environment variables.
 *
 * @param {Logger} logger The logger to attach to.
 */
ConsoleTransport.prototype.attach = function (logger) {
  __attach.call(this, logger);
  this._loggerName = this.loggerScope.name + " -";
};

/**
 * As console does not need children, return the instance.
 *
 * @returns {ConsoleTransport} The instance.
 */
ConsoleTransport.prototype.child = function () { return this; };

/**
 * Writes the record of the given log and the separator to the stream.
 *
 * @param {Log} log The log to write.
 */
ConsoleTransport.prototype.write = function (log) {
  if (this._neglect(log)) { return; }
  log._analyzeArguments();
  const consoleFn = consoleFnByLevel(log.level);
  const levelString = "[" + (valueToName(log.level) + "").toUpperCase() + "]";
  let payload = log.getPayload();
  if (payload === void 0) { payload = ""; }
  if (log._source == null) {
    // [WARN] my-app - message {payload}
    consoleFn.call(console, levelString, this._loggerName, log.getFormattedMessage(), payload);
  } else {
    // [WARN] my-app - message sourceFile:row:col {payload}
    const sourceString = log._source.file + ":" + log._source.row + ":" + log._source.col;
    consoleFn.call(console, levelString, this._loggerName, log.getFormattedMessage(), sourceString, payload);
  }
};
