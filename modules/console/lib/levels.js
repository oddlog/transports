import {MAPPED as LEVELS} from "@oddlog/levels";

/*===================================================== Exports  =====================================================*/

export {consoleFnByLevel};

/*==================================================== Functions  ====================================================*/

function consoleFnByLevel(level) {
  /* eslint-disable no-console */
  if (level < LEVELS.debug) {
    return console.trace;
  } else if (level < LEVELS.info) {
    return console.debug;
  } else if (level < LEVELS.warn) {
    return console.info;
  } else if (level < LEVELS.error) {
    return console.warn;
  } else {
    return console.error;
  }
}
