import ConsoleTransport from "./ConsoleTransport";

/*===================================================== Exports  =====================================================*/

export default ConsoleTransport;

export const plugin = mkPlugin();
export {create, mkPlugin};

/*==================================================== Functions  ====================================================*/

/**
 * Creates a new ConsoleTransport according to given options.
 * TODO add console option to specify the console to use
 *
 * @param {!Object} options The options as accepted by {@link BasicTransport}. Only `shy` is set to `true` by default.
 * @returns {ConsoleTransport} The created transport.
 */
function create(options) { return new ConsoleTransport(options); }

function mkPlugin(options) {
  const overwrite = typeof options === "boolean" ? options : !!(options && options.overwrite);
  return (oddlog) => oddlog.defineType("console", create, overwrite);
}
