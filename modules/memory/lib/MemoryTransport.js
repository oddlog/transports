import BasicTransport from "@oddlog/transport-inherit-basic";
import DeferredTransport from "@oddlog/transport-inherit-deferred";

/*===================================================== Exports  =====================================================*/

export default MemoryTransport;

/*==================================================== Functions  ====================================================*/

/**
 * Creates a new MemoryTransport with provided basic options for basic transports.
 *
 * @param {Object} options The options to pass to {@link BasicTransport} and {@link DeferredTransport}.
 * @param {{push:Function}} store The in-memory store to add messages to.
 * @constructor
 */
function MemoryTransport(options, store) {
  BasicTransport.call(this, options);
  DeferredTransport.call(this, options);
  this.store = store;
}

// extend BasicTransport
MemoryTransport.prototype.minLevel = BasicTransport.prototype.minLevel;
MemoryTransport.prototype.attach = BasicTransport.prototype.attach;
MemoryTransport.prototype._neglect = BasicTransport.prototype._neglect;
MemoryTransport.prototype._isShy = BasicTransport.prototype._isShy;
// extend DeferredTransport
MemoryTransport.prototype.preWrite = DeferredTransport.prototype.preWrite;

/**
 * Creates a child MemoryTransport instance.
 *
 * @returns {MemoryTransport} The child Transport.
 */
MemoryTransport.prototype.child = function () { return new MemoryTransport(this._options, this.store); };

/**
 * Appends the passed log to the store if it's not to be ignored.
 *
 * @param {Log} log The log to process.
 * @see BasicTransport#_neglect
 */
MemoryTransport.prototype.write = function (log) {
  if (this._neglect(log)) { return; }
  this.preWrite(log);
  this.store.push(log);
};
