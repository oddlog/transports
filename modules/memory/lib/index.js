import {create as createCache} from "@oddlog/cache";

import MemoryTransport from "./MemoryTransport";

/*===================================================== Exports  =====================================================*/

export default MemoryTransport;

export const plugin = mkPlugin();
export {create, mkPlugin};

/*==================================================== Functions  ====================================================*/

/**
 * Creates a new in-memory Transport according to given options.
 *
 * @param {Object} options The options:
 * <ul>
 *   <li>`{Array|{push:Function, child:?Function}} [store]` The store to push messages to.</li>
 *   <li>Additional options as accepted by {@link createCache} (if `store` is not set).</li>
 *   <li>Additional options as accepted by {@link BasicTransport}.</li>
 *   <li>Additional options as accepted by {@link DeferredTransport}.</li>
 * </ul>
 * @returns {MemoryTransport} The created transport.
 */
function create(options) {
  let store;
  if (options.hasOwnProperty("store")) {
    store = options.store;
  } else {
    if (!options.hasOwnProperty("limit")) { options.limit = Number.POSITIVE_INFINITY; }
    store = createCache(options);
  }
  return new MemoryTransport(options, store);
}

function check(options) { return options.hasOwnProperty("store"); }

function mkPlugin(options) {
  const overwrite = typeof options === "boolean" ? options : !!(options && options.overwrite);
  return (oddlog) => oddlog.defineType("memory", create, check, overwrite);
}
