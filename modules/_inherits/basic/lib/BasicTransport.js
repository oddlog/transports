import {getLevelValue, MAPPED as LEVELS} from "@oddlog/levels";
import {isAnySet as isEnvSet, isDebugSetAndTest, isTraceSetAndTest} from "@oddlog/environment";

/*===================================================== Exports  =====================================================*/

export default BasicTransport;

/*===================================================== Classes  =====================================================*/

/**
 * Creates a new BasicTransport with given options for basic transports.
 *
 * @param {Object} options The options:
 * <ul>
 *   <li>`{Number|String} [level=fallbackLevel]` The minimum level of logs to accept.</li>
 *   <li>`{Boolean} [shy=logger._shy]` Whether to apply sane defaults for libraries instead of applications.</li>
 * </ul>
 * @param {Number|String} [fallbackLevel] If provided, this will be used instead of DEBUG for the default transport
 *                                        level.
 * @constructor
 */
function BasicTransport(options, fallbackLevel) {
  this._options = options;
  this._fallbackLevel = fallbackLevel;
  this.logger = null;
  this.loggerScope = null;
  this.level = getLevelValue(options.level);
}

/**
 * Returns the minimum level of common logs to be processed by this transport.
 *
 * @returns {Number} The minimum level of logs to be processed by this transport.
 */
BasicTransport.prototype.minLevel = function () { return this.level; };

/**
 * Attaches this transport to the passed logger.
 * This means using that loggers `shy` value as fallback and its name for the environment variables. In addition, the
 * logger is used for event emitting and its {@link LoggerScope} reference (by inheriting transport classes).
 *
 * @param {Logger} logger The logger to attach to.
 */
BasicTransport.prototype.attach = function (logger) {
  this.logger = logger;
  this.loggerScope = logger.scope;

  if (this.level == null) { this.level = this._fallbackLevel; }
  if (isTraceSetAndTest(this.loggerScope.name)) {
    this.level = this.level == null || this.level > LEVELS.trace ? LEVELS.trace : this.level;
  } else if (isDebugSetAndTest(this.loggerScope.name)) {
    this.level = this.level == null || this.level > LEVELS.debug ? LEVELS.debug : this.level;
  } else if (this.level == null) {
    if (this._isShy()) {
      this.level = LEVELS.warn;
    } else if (isEnvSet()) {
      this.level = LEVELS.info;
    } else {
      this.level = LEVELS.debug;
    }
  }
};

/**
 * Logs are ignored if their level is below this transports level option.
 *
 * @param {Log} log The log to check for ignore.
 * @returns {Boolean} Whether the passed log would be ignored by this transport.
 * @private
 */
BasicTransport.prototype._neglect = function (log) { return log._useLevel && log.level < this.level; };

/**
 * Determines whether the transport should be considered being shy.
 *
 * @returns {Boolean} Whether the transport is shy.
 */
BasicTransport.prototype._isShy = function () {
  return this._options._shy || this._options._shy == null && this.logger._shy;
};
