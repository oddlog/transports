import {EOL} from "os";

import StreamTransport from "./StreamTransport";

/*===================================================== Exports  =====================================================*/

export default StreamTransport;

export const plugin = mkPlugin();
export {create, check, mkPlugin};

/*==================================================== Functions  ====================================================*/

/**
 * Creates a new StreamTransport according to given options.
 *
 * @param {Object} options The options:
 * <ul>
 *   <li>`{Stream} [stream=process.stdout]` The stream to write messages to.</li>
 *   <li>`{Boolean} [closeStream=false]` Whether to close the stream when the logger gets closed.</li>
 *   <li>`{String|Function} [format="raw"]` The message format to use; see {@link Log#getRecordString}. Defaults to
 *       "simple" iff logger is set to be `shy` and neither DEBUG nor TRACE environment variables apply.
 *   <li>`{String} [separator=os.EOL]` The separator to add to messages.</li>
 *   <li>`{?String} [encoding]` The encoding to use.</li>
 *   <li>Additional options as accepted by {@link BasicTransport}.</li>
 * </ul>
 * @returns {StreamTransport} The created transport.
 */
function create(options) {
  const stream = options.stream || process.stdout;
  const closeStream = options.closeStream;
  const format = options.format;
  const separator = options.hasOwnProperty("separator") ? options.separator : EOL;
  const encoding = options.encoding;

  return new StreamTransport(options, stream, closeStream, format, separator, encoding);
}

function check(options) { return options.hasOwnProperty("stream"); }

function mkPlugin(options) {
  const overwrite = typeof options === "boolean" ? options : !!(options && options.overwrite);
  return (oddlog) => oddlog.defineType("stream", create, check, overwrite);
}
