import {isAnySetAndTest as isEnvMatching} from "@oddlog/environment";
import BasicTransport from "@oddlog/transport-inherit-basic";

/*===================================================== Exports  =====================================================*/

export default StreamTransport;

/*===================================================== Classes  =====================================================*/

/**
 * Creates a new StreamTransport that writes log records into a stream.
 *
 * @param {Object} options The options to pass to {@link BasicTransport}.
 * @param {Stream} stream The stream to write messages to.
 * @param {Boolean} closeStream Whether to close the stream on {@link LoggerScope#close}.
 * @param {?String|Function} format The message format to use; see {@link Log#getRecordString}.
 * @param {?String} separator The separator to append to each message. Defaults to `EOL`.
 * @param {?String} encoding The encoding to use for the stream.
 * @constructor
 */
function StreamTransport(options, stream, closeStream, format, separator, encoding) {
  BasicTransport.call(this, options);
  this._options = options;
  this.stream = stream;
  this.closeStream = closeStream;
  this.separator = separator;
  this._format = format;
  this._encoding = encoding;

  this.writeCb = () => this.loggerScope.up();

  if (this._encoding != null) { this.stream.setDefaultEncoding(this._encoding); }
}

// extend BasicTransport
StreamTransport.prototype.minLevel = BasicTransport.prototype.minLevel;
StreamTransport.prototype._neglect = BasicTransport.prototype._neglect;
StreamTransport.prototype._isShy = BasicTransport.prototype._isShy;

/**
 * Attaches this transport to the passed logger.
 *
 * @param {Logger} logger The logger to attach to.
 * @see BasicTransport#attach
 */
StreamTransport.prototype.attach = function (logger) {
  BasicTransport.prototype.attach.call(this, logger);
  const name = this.loggerScope.name;
  if (this._format != null) {
    this.format = this._format;
  } else {
    this.format = this._isShy() && !isEnvMatching(name) ? "simple" : "raw";
  }
  if (this.closeStream) { this.loggerScope.once("close", () => this._endStream()); }
};

/**
 * Creates a child StreamTransport instance.
 *
 * @returns {StreamTransport} The child Transport.
 */
StreamTransport.prototype.child = function () {
  return new StreamTransport(this._options, this.stream, false, this._format, this.separator, this._encoding);
};

/**
 * Writes the record of the given log and the separator to the stream.
 *
 * @param {Log} log The log to write.
 */
StreamTransport.prototype.write = function (log) {
  if (this._neglect(log)) { return; }
  this.loggerScope.down();
  this.stream.write(log.getRecordString(this.format) + this.separator, this.writeCb);
};

/**
 * End the underlying stream.
 * @private
 */
StreamTransport.prototype._endStream = function () {
  this.loggerScope.down();
  this.stream.end(this.writeCb);
};
