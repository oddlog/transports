import fs from "fs";
import path from "path";
import uuid from "uuid/v4";

import {createLogger} from "oddlog";

const TMP_DIR = path.resolve(__dirname, ".tmp");

describe("FileTransport", () => {
  beforeEach((done) => {
    fs.access(TMP_DIR, fs.R_OK, (err) => {
      if (err == null) { return done(); }
      fs.mkdir(TMP_DIR, done);
    });
  });
  after((done) => {
    fs.access(TMP_DIR, fs.R_OK, (err) => err == null ? fs.rmdir(TMP_DIR, done) : done());
  });

  let file, logger;

  beforeEach(() => {
    file = path.join(TMP_DIR, uuid());
    logger = createLogger("some-id", {transports: [{path: file}]}, {hello: "world"});
  });
  afterEach((done) => fs.unlink(file, done));
  afterEach((done) => logger.scope.closed ? done() : logger.close(done));

  it("should create a file", (done) => {
    logger.scope.once("drain", () => {
      fs.exists(file, (bool) => {
        bool.should.be.true();
        done();
      });
    });
    logger.info("test");
  });
});
