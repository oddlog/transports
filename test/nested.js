import {createLogger, INFO, TRACE} from "oddlog";

describe("NestedTransport (TriggerTransport)", () => {
  let store0, store1, logger;

  beforeEach(() => {
    store0 = [];
    store1 = [];
    logger = createLogger("some-id", {
      transports: [{level: TRACE, trigger: INFO, guard: false, transports: [{store: store0}, {store: store1}]}]
    }, {hello: "world"});
  });
  afterEach((done) => logger.scope.closed ? done() : logger.close(done));

  it("should forward messages to all transports", (done) => {
    store0.length.should.be.equal(0);
    store1.length.should.be.equal(0);
    logger.info("test");
    logger.close(() => {
      store0.length.should.be.equal(1);
      store1.length.should.be.equal(1);
      done();
    });
  });
});
