import {mixin} from "oddlog";
import {mkPlugin as mkStreamTransportPlugin} from "../modules/stream/lib/index";
import {plugin as memoryTransportPlugin} from "../modules/memory/lib/index";
import {plugin as fileTransportPlugin} from "../modules/file/lib/index";
import {plugin as splitTransportPlugin} from "../modules/split/lib/index";
import {plugin as triggerTransportPlugin} from "../modules/trigger/lib/index";

mixin([
  mkStreamTransportPlugin(true),
  memoryTransportPlugin,
  fileTransportPlugin,
  splitTransportPlugin,
  triggerTransportPlugin,
]);
